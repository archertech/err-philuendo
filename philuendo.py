
import random
from errbot import botcmd, BotPlugin

class Philuendo(BotPlugin):

    quotes = [
        [
            "You get the FictiveKin secret sauce...",
            "...if it's tasty to you.",
        ],
        "We are one body, one mind",
    ]

    @botcmd
    def philuendo(self, msg, args):
        """ Shows a quote from Phil.
        Example: !philism
        """

        channel_id = self.build_identifier(str(msg.to))
        quote = self.quotes[random.randint(0, len(self.quotes))-1]
        if isinstance(quote, list):
            for part in quote:
                self.send(channel_id, part.format(user=str(msg.frm.nick)))

        else:
            self.send(channel_id, quote.format(user=str(msg.frm.nick)))
